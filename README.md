# Audio 2 Text Summary
This is a python script that takes any youtube or local video/audio file, transcripts it using whisper, and pipes it into a LLM using ollama to get a summary.

## Installation
Install requirements, make sure you also have [ollama](https://ollama.com/) installed on your machine!
```bash
pip install -r requirements.txt
```

## Usage
```bash
python3 ./summary.py [LINK]
```
or
```bash
python3 ./summary.py [PATH-TO-FILE]
```
To use a file path, make sure the path to the file has no spaces!

## ToDo
- Add support for other online links.
- Add GUI?