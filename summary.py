import sys,os
import ollama
import whisper
import yt_dlp

def transcribe_video(url):
    if not url.startswith("http"):
        return wmodel.transcribe(url)
    else:
        yt_opts = {'extract_audio': True, 'format': 'bestaudio', 'outtmpl': 'audio.mp3'}
        with yt_dlp.YoutubeDL(yt_opts) as ydl:
            ydl.download(url)
            return wmodel.transcribe("audio.mp3")
            os.remove("audio.mp3")

# Set up the youtube video URL
url = sys.argv[1] if len(sys.argv) > 1 else "https://www.youtube.com/watch?v=dQw4w9WgXcQ"

# Transcribe the video using whisper and delete audio.mp3
wmodel = whisper.load_model("base")
transcription = transcribe_video(url)

# Pipes the transcription into an LLM model using ollama to get a summary
response = ollama.chat(model='llama2', messages=[
    {
        'role': 'system',
        'content': 'You are a helpful assistant that only provides a summary of whatever text is provided.',
    },
    {
        'role': 'user',
        'content': transcription["text"],
    }
])
print(response['message']['content'])